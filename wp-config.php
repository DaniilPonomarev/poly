<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
	
define('WPCF7_AUTOP', false); 

// ** MySQL settings ** // 
/** The name of the database for WordPress */
define( 'DB_NAME', "cw97450_polybox" );

/** MySQL database username */
define( 'DB_USER', "cw97450_polybox" );

/** MySQL database password */
define( 'DB_PASSWORD', "cw97450_polybox" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'nX?$6itdoE/{3GLkILp(/_]{P0<b>3.|bjd?*z`?_jA:yu,*2/X]4+5S!>!%#IE}' );
define( 'SECURE_AUTH_KEY',  'Mitrjv`%}Th1`x&<-o15~&5^>.uaWV)r}4n~*[d^ep~fw?L[x-MR^<TD gU.8@ec' );
define( 'LOGGED_IN_KEY',    'vA4V)@&XGP]l-_k_j3A|N]dr`dA)$|1l*:/887M#QuHLyb?sf8R0X-UD~Z;g F7!' );
define( 'NONCE_KEY',        'Q:!5d--,#C6W@Zx>ae4805J-V`@qV>T|0v7?:X(_|;cj}XhaY<rBQ)<$adx0Q#k|' );
define( 'AUTH_SALT',        '1E9t]0%cODbwnDxU;<d-]>~TiQ2a~g7AmP><-PtEVsIC[LFGMzD|cUL>3BXH:6*4' );
define( 'SECURE_AUTH_SALT', 'cA#bGtP]Iol%:1Z>waV?/]a,Z?IK QE2AsG7i5+25],uZ@/ye9c6.zYXRYjedV].' );
define( 'LOGGED_IN_SALT',   'sN B85ERSk/1z@}4;bPM:0mR%2#EEf>:tnd]TH9la@Ij@>X<&3|nEos B#9bGE;P' );
define( 'NONCE_SALT',       '!R>`L3rgrfNKRbJc-[ktT*A&^9r&mhuHrS369t9M^M2+R#F:x>:]dM!{9C~^,j{b' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'pb_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
