<?php
/**
 * The theme header
 * 
 * @package delfi
 */
GLOBAL $site_settings;
$du = get_template_directory_uri();
$title = is_category() ? single_cat_title('', false) : get_the_title();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400i,400,500,700&amp;subset=cyrillic" rel="stylesheet">
	<?php wp_head(); ?>
	<style>.block-header .block-inner .top .block-menu .inner ul li.logo a {background-image:url(<?=$site_settings['logo']?>)}</style>
</head>
<body <?php body_class(); ?>>
    <header class="block block-header <?=is_front_page()?'block-promo':'block-header-inner'?>">
        <div class="block-inner">
			<!--noindex-->
            <div class="top">
                <div class="show-mobile--flex mobile-icons">
                    <a href="" class="menu toggle-menu"></a>
                    <a class="phone1" class="call"></a>
                    <a class="phone2" class="call"></a>
                </div>
                <div class="block-soc">
                    <div class="inner">
					<?php if(!empty($site_settings['fb'])) { ?>
						<a href="<?=$site_settings['fb']?>" class="fb" title="Мы в Facebook" target="_blank">
							<img src="<?=$du?>/assets/images/fb.svg" alt="Мы в Facebook">
						</a>
					<?php } ?>
					<?php if(!empty($site_settings['vk'])) { ?>
						<a href="<?=$site_settings['vk']?>" class="vk" title="Мы в VK" target="_blank">
							<img src="<?=$du?>/assets/images/vk.svg" alt="Мы в VK">
						</a>
					<?php } ?>
					<?php if(!empty($site_settings['twitter'])) { ?>
						<a href="<?=$site_settings['twitter']?>" class="tw" title="Мы в Twitter" target="_blank">
							<img src="<?=$du?>/assets/images/tw.svg" alt="Мы в Twitter">
						</a>
					<?php } ?>	
					<?php if(!empty($site_settings['insta'])) { ?>
						<a href="<?=$site_settings['insta']?>" class="tw" title="Мы в Instagtram" target="_blank">
							<img src="<?=$du?>/assets/images/instagram.svg" alt="Мы в Instagtram">
						</a>
					<?php } ?>
                    </div>
                </div>
                <div class="block-contacts">
                    <div class="inner">
                        <div class="phone">
                            <a class="phone1" href="tel:<?=$site_settings['phone_tel']?>"><?=$site_settings['phone']?></a>
                        </div>
                        <div class="phone">
                            <a class="phone2" href="tel:<?=$site_settings['phone2_tel']?>"><?=$site_settings['phone2']?></a>
                        </div>
                        <div class="phone phone__email">
                            <a href="mailto:<?=$site_settings['email']?>"><?=$site_settings['email']?></a>
                        </div>
                        <div class="time"><?=nl2br($site_settings['time'])?></div>
                    </div>
                </div>
                <div class="block-menu">
                    <div class="inner">
                        <div class="show-mobile top-panel">
                            <a href="#" class="toggle-menu close-menu">&nbsp;</a>
                            <a class="phone show-mobile phone1" href="tel:<?=$site_settings['phone_tel']?>"><?=$site_settings['phone']?></a>
                            <a class="phone show-mobile phone2" href="tel:<?=$site_settings['phone2_tel']?>"><?=$site_settings['phone2']?></a>
                            <a class="phone show-mobile" href="mailto:<?=$site_settings['email']?>"><?=$site_settings['email']?></a>
                        </div>
						<div class="middle-panel">
							<?php wp_nav_menu(['theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav']); ?> 
                        </div>
                        <div class="show-mobile--flex bottom-panel">
                             <a href="/">
                                <img src="<?=$site_settings['logo']?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="show-mobile--flex mobile-logo">
                    <a href="/">
						<img src="<?=$site_settings['logo']?>" alt="">
					</a>
                </div>
            </div>
			
		
			<!--/noindex-->
			<?php if(is_front_page()) { ?>
            <div class="middle" id="block-middle">
                <div class="left">
                    <p class="title">Изготовление изделий</p>
                    <p class="subtitle">из листового полипропилена</p>
                    <a href="<?=get_permalink(424)?>" class="btn btn-transparent animate-to">Примеры готовых работ</a>
                    <!--
	                <div class="show-tablet">
                        <a href="#block-calc" class=" btn btn-transparent animate-to">Заказать коробку</a>
                    </div>
					-->
                </div>
                <div class="center">
                    <img src="<?=$du?>/assets/images/promo-box.png" alt="">
                </div>
                <div class="right">
                    <p>по любым размерам и чертежам от 1 шт</p>
                    <a href="#block-calc" class="btn btn-transparent animate-to">Заказать коробку</a>
                </div>
            </div> 
            <div class="bottom">
                <a href="#block-calc" class="next animate-to">
                    Конструктор коробок
                    <img src="<?=$du?>/assets/images/arrow-down.svg" alt="">
                </a>
            </div>
			<?php } else { ?>
			<div class="bottom bottom-page-info">
				<h1 class="page-title"><?=$title?></h1>
				<!--noindex-->
				<div class="breadcrumbs">
					<?=delfiBreadcrumbs()?>
				</div>
				<div class="bg<?=is_category()?'-2':''?>"></div>
				<!--/noindex-->
			</div>
			<?php } ?>
        </div>
		<!--noindex-->
		<div class="d-print head-print">
			<table>
				<tr>
					<td class="left-info">
						<p class="site-address">http://upakovka.poly-box.ru</p>
						<p class="phone"><?=$site_settings['address']?></p>
						<p class="time"><?=nl2br($site_settings['time'])?></p>
					</td>
					<td class="logo">
						<img src="<?=$du?>/assets/images/logo_print.svg" alt="">
					</td>
					<td class="info">
						<p class="phone phone1"><?=$site_settings['phone']?></p>
						<p class="phone phone2"><?=$site_settings['phone2']?></p>
						<p class="phone phone__email"><?=$site_settings['email']?></p>
					</td>
				</tr>
			</table>
			
		</div>
		<!--/noindex-->
    </header>
	<main>