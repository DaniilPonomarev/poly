<?php
/**
 * Delfi theme
 * 
 * @package delfi
 */

DEFINE('THEME_VERSION', '0.6.11');

add_image_size('gallery_thumb', 600, 400, array('center', 'center'));

if (!function_exists('delfiSetup')) {
	function delfiSetup() {
		add_theme_support('post-thumbnails');
		register_nav_menus(array(
			'primary' => __('Primary Menu', 'delfi'),
		));
	}
} 
add_action('after_setup_theme', 'delfiSetup');

if (!function_exists('delfiEnqueueScripts')) {
	function delfiEnqueueScripts() {
		GLOBAL $site_settings;
		$du = get_template_directory_uri();
		wp_enqueue_style('main-style', $du . '/assets/css/main.css', array(), THEME_VERSION);
		wp_enqueue_style('swiper-style', $du . '/assets/vendor/swiper/css/swiper.min.css', array(), THEME_VERSION);
		wp_enqueue_style('fancybox-style', $du . '/assets/vendor/fancybox-master/dist/jquery.fancybox.min.css', array(), THEME_VERSION);
		wp_enqueue_style('rangeSlider-style', $du . '/assets/vendor/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css', array(), THEME_VERSION);
		wp_enqueue_style('rangeSlider-skin-style', $du . '/assets/vendor/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinModern.css', array(), THEME_VERSION);

		
		wp_enqueue_script('jquery');
		wp_enqueue_script('rangeSlider-script', $du . '/assets/vendor/ion.rangeSlider-2.2.0/js/ion.rangeSlider.min.js', [], THEME_VERSION, true);
		wp_enqueue_script('swiper-script', $du . '/assets/vendor/swiper/js/swiper.min.js', [], THEME_VERSION, true);
		wp_enqueue_script('fancybox-script', $du . '/assets/vendor/fancybox-master/dist/jquery.fancybox.min.js', [], THEME_VERSION, true);
		wp_enqueue_script('main-script', $du . '/assets/main.js', array('jquery'), THEME_VERSION, true);
		
		if(is_front_page()) {
			wp_enqueue_script('html2canvas-script', $du . '/assets/vendor/html2canvas.min.js', array('jquery'), THEME_VERSION, true);
			wp_enqueue_script('calc-script', $du . '/assets/calc.js', array('main-script', 'jquery'), THEME_VERSION, true);
		}
		
		//wp_enqueue_script('googlemap-script', 'https://maps.googleapis.com/maps/api/js?key=' . $site_settings['google_key'] . '&callback=initMap', ['main-script'], THEME_VERSION, true);
		wp_enqueue_style('delfi-style', get_stylesheet_uri(), array(), THEME_VERSION);
	}// delfiEnqueueScripts
}
add_action('wp_enqueue_scripts', 'delfiEnqueueScripts');


function my_acf_google_map_api( $api ){
	GLOBAL $site_settings;
	$api['key'] = $site_settings['google_key'];
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/headers.php';

GLOBAL $site_settings;

$site_settings = [
	'google_key' 	=> get_theme_mod('google_key'),
	'phone' 		=> get_theme_mod('phone'),
	'phone2' 		=> get_theme_mod('phone2'),
	'email' 		=> get_theme_mod('email'),
	'time' 			=> get_theme_mod('time'),
	'copyright' 	=> get_theme_mod('copyright'),
	'logo' 			=> get_theme_mod('logo'),
	'fb' 			=> get_theme_mod('fb'),
	'vk' 			=> get_theme_mod('vk'),
	'twitter' 		=> get_theme_mod('twitter'),
	'insta' 		=> get_theme_mod('insta'),
	'lat' 			=> get_theme_mod('lat'),
	'lng' 			=> get_theme_mod('lng'),
	'zoom' 			=> get_theme_mod('zoom'),
	'address' 		=> get_theme_mod('address'),
];

$site_settings['phone_tel']	= preg_replace('/[^\+0-9]/', '', $site_settings['phone']);
$site_settings['phone2_tel']= preg_replace('/[^\+0-9]/', '', $site_settings['phone2']);

function isPS() {
	$res = !(strstr($_SERVER['HTTP_USER_AGENT'], 'Google Page Speed Insights') == false);
	$res = $res || !(strstr($_SERVER['HTTP_USER_AGENT'], 'PageSpeed') == false);
	$res = $res || !(strstr($_SERVER['HTTP_USER_AGENT'], 'GTmetrix') == false);
	$res = $res || !(strstr($_SERVER['HTTP_USER_AGENT'], 'YSlow') == false);
	$res = $res || !(strstr(getUserIp(), '204.187.') == false);
	$res = $res || !(strstr(getUserIp(), '66.249.') == false);
	return $res;
}


function getUserIp() {
	if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
		if(isset($_SERVER["HTTP_CLIENT_IP"])) {
			return $_SERVER["HTTP_CLIENT_IP"];
		} else {
			return $_SERVER["REMOTE_ADDR"];
		}
		
		return $_SERVER["HTTP_X_FORWARDED_FOR"];
	} else {
		if(isset($_SERVER["HTTP_CLIENT_IP"])) {
			return $_SERVER["HTTP_CLIENT_IP"];
		} else {
			return $_SERVER["REMOTE_ADDR"];			
		}
	}
}

function delfiChangePagesize($query) {
    if (is_category('news')) {
        $query->query_vars['posts_per_page'] = 100;
    }
}
add_action('pre_get_posts', 'delfiChangePagesize', 1 );


/**
 * Custom WP gallery
 */
function delfi_gallery_shortcode($attr) {
   $post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
	    // 'ids' is explicitly ordered, unless you specify otherwise.
	    if ( empty( $attr['orderby'] ) )
	        $attr['orderby'] = 'post__in';
	    $attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
	    return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
	    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
	    if ( !$attr['orderby'] )
	        unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
	    'order'      => 'ASC',
	    'orderby'    => 'menu_order ID',
	    'id'         => $post->ID,
	    'itemtag'    => 'li',
	    'icontag'    => 'figure',
	    'captiontag' => 'figcaption',
	    'columns'    => 3,
	    'size'       => 'thumbnail',
	    'include'    => '',
	    'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
	    $orderby = 'none';

	if ( !empty($include) ) {
	    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

	    $attachments = array();
	    foreach ( $_attachments as $key => $val ) {
	        $attachments[$val->ID] = $_attachments[$key];
	    }
	} elseif ( !empty($exclude) ) {
	    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
	    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
	    return '';

	if ( is_feed() ) {
	    $output = "\n";
	    foreach ( $attachments as $att_id => $attachment )
	        $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
	    return $output;
	}

	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	$size_class = sanitize_html_class( $size );
	$gallery_div = "
				<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	//var_dump($attachments);
	foreach ( $attachments as $id => $attachment ) {
		$img_src = wp_get_attachment_image_src($id, 'gallery_thumb', false);
		$image_alt = get_post_meta( $id, '_wp_attachment_image_alt', true);
	    $output .= "
					<figure class='gallery-item'>
						<a href='" . wp_get_attachment_url($id) . "' title='" . esc_attr($attachment->post_title) . "'>
							<img src='{$img_src[0]}' alt='" . esc_attr($image_alt) . "' />
		";
	    if (trim($attachment->post_excerpt)) {
	        $output .= "
							<figcaption class='wp-caption-text gallery-caption'>" 
								. nl2br(wptexturize($attachment->post_excerpt)) . "
							</figcaption>";
	    }
	    $output .= "
						</a>
					</figure>";
	}

	$output .= "
				</div>\n";

	return $output;
}

remove_shortcode('gallery');
add_shortcode('gallery', 'delfi_gallery_shortcode');    

function my_gallery_default_type_set_link( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
    return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');