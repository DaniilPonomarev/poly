<?php
/**
 * The main template file
 * 
 * @package delfi
 */

get_header();

$years = array();

if( have_posts() ) {
	
	function tag_map($val) {
		return $val->name;
	}
	
	while(have_posts()) {
		the_post();
		$year = get_the_date('Y');
		if(!isset($years[$year])) {
			$years[$year] = [];
		}
		
		$years[$year][] = [
			'title'	 	=> get_the_title(), 
			'permalink' => get_the_permalink(),
			'thumbnail' => get_the_post_thumbnail_url(get_the_ID(), 'medium_large'),
			'date'		=> get_the_date('d F \'y', $item->ID),
			'tags'		=> implode(', ', array_map('tag_map' , wp_get_post_tags(get_the_ID())))
		];
    }
}

?>
<?php foreach($years as $year => $items) { ?>
	<div class="block-news-items">
		<div class="block-inner">
			<p class="year"><?=$year?></p>
			<div class="items">
			<?php foreach($items as $item) { ?>
				<a href="<?=$item['permalink']?>" class="item">
					<div class="item-inner">
						<div class="img" style="background-image:url(<?=$item['thumbnail']?>)"></div>
						<p class="title"><?=$item['title']?></p>
						<div class="item-footer">
							<span class="date"><?=$item['date']?></span>
							<?php if(!empty($item['tags'])) { ?>
							<span class="divider">•</span>
							<span class="category"><?=$item['tags']?></span>
							<?php } ?>
						</div>
					</div>
				</a>
			<?php } ?>
			</div>
			<!--
			<div class="load-more">
				<a href="#">Показать еще</a>
			</div>
			-->
		</div>
	</div>
<?php } ?>

<?php include_once('blocks/_consult.php'); ?>
<?php include_once('blocks/_map.php'); ?>


<?php get_footer(); ?> 