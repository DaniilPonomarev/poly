<?php
/**
 * The main template file
 * 
 * @package delfi
 */

get_header();
?>
<main id="main" class="site-main" role="main">
<?php 
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			get_template_part('content', get_post_format());
		}// end while
		delfiPagination();
	} else {
		get_template_part('no-results', 'index'); 
	} // endif; 
?> 
</main>
<?php get_footer(); ?> 