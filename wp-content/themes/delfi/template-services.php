<?php
	/** Template Name: Услуги */
	get_header();
?>

<div class="block block-services block block-services__white" id="block-services">
	<div class="block-inner">
		<?php 
			$services_groups = get_children([
				'post_status' => 'publish', 
				'post_parent' => 34,
				'orderby'     => 'menu_order',
				'order'       => 'ASC',
			]);
			foreach($services_groups as $service_group) {
		?>
		<div class="service-group">
			<p class="sg-title"><?=$service_group->post_title?>:</p>
			<div class="items">	
				<?php 
					$items = get_children([
						'post_status' => 'publish', 
						'post_parent' => $service_group->ID,
						'orderby'     => 'menu_order',
						'order'       => 'ASC',
					]);
					
					foreach($items as $item) {
						$img = get_the_post_thumbnail_url($item, 'thumbnail');
				?>
				<a href="<?=get_the_permalink($item->ID)?>" class="item">
					<span class="img">
						<img src="<?=$img?>" alt="<?=esc_attr($item->post_title)?>">
					</span>
					<span class="title"><?=$item->post_title?></span>
				</a>
				<?php } //$items ?>
			</div>
		</div>

		<?php } //$services_groups ?>
	
		<a href="#make-consultation" class="btn btn-transparent btn-transparent-inverse fancybox">Консультация</a>
	</div>
</div>

<?php
	get_footer();
?>