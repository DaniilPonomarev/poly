<?php
/**
 * The theme footer
 * 
 * @package delfi
 */
GLOBAL $site_settings;
$du = get_template_directory_uri();
?>
 </main>
	<!--noindex-->
    <footer class="block block-footer">
        <div class="block-inner">
            <p class="description">
				<?=nl2br($site_settings['copyright'])?>
            </p>
            <p class="copyright">
                Copyright &copy; <?=date('Y')?>
                <a href="<?=get_the_permalink(87)?>">Политика конфиденциальности</a>
            </p>
            <div class="block-soc">
                <div class="inner">
				<?php if(!empty($site_settings['fb'])) { ?>
					<a href="<?=$site_settings['fb']?>" class="fb" title="Мы в Facebook">
						<img src="<?=$du?>/assets/images/fb.svg" alt="Мы в Facebook">
					</a>
				<?php } ?>
				<?php if(!empty($site_settings['vk'])) { ?>
					<a href="<?=$site_settings['vk']?>" class="vk" title="Мы в VK">
						<img src="<?=$du?>/assets/images/vk.svg" alt="Мы в VK">
					</a>
				<?php } ?>
				<?php if(!empty($site_settings['twitter'])) { ?>
					<a href="<?=$site_settings['twitter']?>" class="tw" title="Мы в Twitter">
						<img src="<?=$du?>/assets/images/tw.svg" alt="Мы в Twitter">
					</a>
				<?php } ?>
                </div>
            </div>
        </div>
    </footer>
    <div class="modals hide">
        <div class="modal modal-order" id="make-order">
            <div class="modal-inner">
                <p class="modal-title">Оформить заказ</p>
                <!--<p class="modal-subtitle">Наш менеджер свяжется с Вами в течение 2 часов</p>-->
                <div class="cols box-cols line">
                    <div class="col col-1 width-1-2">
                        <div class="col-inner">
                            <div id="order-box">&nbsp;</div>
                        </div>
                    </div>
                    <div class="col col-2 width-1-2">
                        <div class="col-inner">
                            <div class="col-text"></div>
							
							<a href="#" class="show-desktop--ib btn btn-transparent do-print">Распечатать</a>
                        </div>
                    </div>
                </div>
				<?=do_shortcode('[contact-form-7 id="170" title="Оформить заказ (всплывающее окно)"]')?>
            </div>
        </div>
		<?php /*
		<div class="modal modal-order" id="make-consultation">
            <div class="modal-inner">
                <p class="modal-title">Консультация</p>
                <p class="modal-subtitle">Наш менеджер свяжется с Вами в течение 2 часов</p>
				<br/>
				<?=do_shortcode('[contact-form-7 id="173" title="Консультация (всплывающее окно)"]')?>
            </div>
        </div>
		*/ ?>
    </div>
	<!--/noindex-->
	<?php wp_footer(); ?> 
	<?php if(!isPS()) {?>
	<!--noindex-->
	<script type='text/javascript'> 
	(function(){ var widget_id = 'WpR7fV4oMa';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})(); 
	</script> 
	<!--/noindex-->
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(49284364, "init", {
			id:49284364,
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true,
			webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/49284364" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<?php } ?>
</body>
</html>