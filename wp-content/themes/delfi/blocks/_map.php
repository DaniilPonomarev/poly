<?php 
	if(!isPS()) {
	GLOBAL $site_settings; 
	$data_address = $site_settings['address']
					. "<br>" . 'тел. <a class="phone1" href="tel:' . $site_settings['phone_tel'] . '">' . $site_settings['phone'] . '</a>,'
					. "<br>" . 'email: <a href="mailto:' . $site_settings['email'] . '">' . $site_settings['email'] . '</a>'
					;
?>
<div class="block block-contacts" id="block-contacts">
	<div class="block-inner">
		<p class="block-title">Контакты</p>
		<div class="map-description inner">
			<p><?=$data_address?></p>
		</div>
	</div>
	<div class="block-inner">
		<a href="yandexnavi://build_route_on_map?lat_to=59.655805&lon_to=30.560557" class="btn btn-transparent btn-transparent-inverse show-mobile show-tablet">Маршрут в&nbsp;Яндекс.Навигаторе</a>
        <div class="map" id="contacts-map">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A25b764f05f73a7bc4b810fdc673174001b7aa39a49c625d01d5680acdcf72684&amp;width=100%25&amp;height=350&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
		<? /*
		<div id="contacts-map" class="map" data-lat="<?=$site_settings['lat']?>" data-lng="<?=$site_settings['lng']?>" data-zoom="<?=$site_settings['zoom']?>"
			data-address="<?=esc_attr($data_address)?>"></div>
		*/ ?>
	</div>
</div>
<?php }?> 