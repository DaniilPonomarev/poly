<?php 
	if(!isPS()) {
	GLOBAL $site_settings; 
	$data_address = $site_settings['address']
					. "<br>" . 'тел. <a href="tel:' . $site_settings['phone_tel'] . '">' . $site_settings['phone'] . '</a>,'
					. "<br>" . 'email: <a href="mailto:' . $site_settings['email'] . '">' . $site_settings['email'] . '</a>'
					;
?>
<div class="block block-contacts" id="block-contacts">
	<div class="block-inner">
		<!-- <a href="" class="btn btn-transparent btn-transparent-inverse">Контакты</a> -->
		<div id="contacts-map" class="map" data-lat="<?=$site_settings['lat']?>" data-lng="<?=$site_settings['lng']?>" data-zoom="<?=$site_settings['zoom']?>"
			data-address="<?=esc_attr($data_address)?>"></div>
	</div>
</div>
<?php }?>