<?php
	add_action('customize_register', function($customizer) {
		$customizer->add_section('section_one', array('title' => 'Настройки сайта','description' => '','priority' => 1));
		
		$customizer->add_setting('phone', array('default' => '+7 (812) 313-75-50'));
		$customizer->add_control('phone', array('label' => 'Телефон','section' => 'section_one','type' => 'text',));
		
		$customizer->add_setting('phone2', array('default' => '8-800-350-69-51'));
		$customizer->add_control('phone2', array('label' => 'Телефон 2','section' => 'section_one','type' => 'text',));
		
		$customizer->add_setting('time', array('default' => 'пн.-пт. с 09-30 до 18-00\nсб.-вс. - выходные'));
		$customizer->add_control('time', array('label' => 'Воемя работы','section' => 'section_one','type' => 'textarea',));
		
		$customizer->add_setting('address', array('default' => ''));
		$customizer->add_control('address', array('label' => 'Адрес','section' => 'section_one','type' => 'text'));
		
		$customizer->add_setting('email', array('default' => ''));
		$customizer->add_control('email', array('label' => 'Email','section' => 'section_one','type' => 'text'));
			
		$customizer->add_setting('lat', array('default' => ''));
		$customizer->add_control('lat', array('label' => 'Широта','description' => 'lat для карты','section' => 'section_one','type' => 'text'));
		
		$customizer->add_setting('lng', array('default' => ''));
		$customizer->add_control('lng', array('label' => 'Долгота','description' => 'lng для карты','section' => 'section_one','type' => 'text'));
		
		$customizer->add_setting('zoom', array('default' => '12'));
		$customizer->add_control('zoom', array('label' => 'Зум','description' => 'для карты','section' => 'section_one','type' => 'number'));
		
		$customizer->add_setting('copyright', array('default' => 'Производство и продажа коробок из полипропилена\nООО «Пластмастер Спб». ИНН 21123324255'));
		$customizer->add_control('copyright', array('label' => 'Копирайт','section' => 'section_one','type' => 'textarea',));
		
		$customizer->add_setting('logo'); 
		$customizer->add_control(new WP_Customize_Image_Control($customizer, 'logo', array('label'    => 'Логотип','section'  => 'section_one','settings' => 'logo',)));
		
		$customizer->add_setting('fb', array('default' => ''));
		$customizer->add_control('fb', array('label' => 'Ссылка Facebook','section' => 'section_one','type' => 'text',));
		$customizer->add_setting('vk', array('default' => ''));
		$customizer->add_control('vk', array('label' => 'Ссылка VK','section' => 'section_one','type' => 'text',));
		
		$customizer->add_setting('twitter', array('default' => ''));
		$customizer->add_control('twitter', array('label' => 'Ссылка Twitter','section' => 'section_one','type' => 'text',));
		
		$customizer->add_setting('insta', array('default' => ''));
		$customizer->add_control('insta', array('label' => 'Ссылка Instagram','section' => 'section_one','type' => 'text',));
		
		$customizer->add_setting('google_key', array('default' => ''));
		$customizer->add_control('google_key', array('label' => 'API Ключ гугл карт','section' => 'section_one','type' => 'text',));
		
	});
?>