<?php
/**
 * ��������� HTTP ��������� Last-Modified
 * ��� ��������� �������, � ���� �������������� ������ � HTTP-���������� �������� Last-Modified
 * https://sheensay.ru/?p=247
 */
 
 	/*
	if(!is_user_logged_in()) {
		$LastModified = get_the_modified_date('r');
		$IfModifiedSince = false;
		if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
			$IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5)); 
		}
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
			$IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
		}
		if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
			exit;
		}
		header('Last-Modified: '. $LastModified);
	}
	*/
 
add_action( 'template_redirect', 'Sheensay_HTTP_Headers_Last_Modified' );
 
function Sheensay_HTTP_Headers_Last_Modified() {
 
    if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || ( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST ) || ( defined( 'REST_REQUEST' ) && REST_REQUEST ) || ( is_admin() ) ) {
        return;
    }
 
    $last_modified = '';
 
    // ��� ������� � �������
    if ( is_singular() ) {
        global $post;
 
        // ���� ���� ��������� - ���������� ���
        if ( post_password_required( $post ) )
            return;
 
        if ( !isset( $post -> post_modified_gmt ) ) {
            return;
        }
 
        $post_time = strtotime( $post -> post_modified_gmt );
        $modified_time = $post_time;
 
        // ���� ���� �����������, ��������� ����
        if ( ( int ) $post -> comment_count > 0 ) {
            $comments = get_comments( array(
                'post_id' => $post -> ID,
                'number' => '1',
                'status' => 'approve',
                'orderby' => 'comment_date_gmt',
                    ) );
            if ( !empty( $comments ) && isset( $comments[0] ) ) {
                $comment_time = strtotime( $comments[0] -> comment_date_gmt );
                if ( $comment_time > $post_time ) {
                    $modified_time = $comment_time;
                }
            }
        }
 
        $last_modified = str_replace( '+0000', 'GMT', gmdate( 'r', $modified_time ) );
    }
 
 
    // C������� �������: �������, �����, ���� � ���� ��������
    if ( is_archive() || is_home() ) {
        global $posts;
 
        if ( empty( $posts ) ) {
            return;
        }
 
        $post = $posts[0];
 
        if ( !isset( $post -> post_modified_gmt ) ) {
            return;
        }
 
        $post_time = strtotime( $post -> post_modified_gmt );
        $modified_time = $post_time;
 
        $last_modified = str_replace( '+0000', 'GMT', gmdate( 'r', $modified_time ) );
    }
 
 
    // ���� ��������� ��� ���������� - ������ �� ������
    if ( headers_sent() ) {
        return;
    }
 
    if ( !empty( $last_modified ) ) {
        if ( !is_user_logged_in() ) {
		header( 'Last-Modified: ' . $last_modified );
            if ( isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) && strtotime( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) >= $modified_time ) {
                $protocol = (isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
                header( $protocol . ' 304 Not Modified' );
            }
        }
    }
}