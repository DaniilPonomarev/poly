<?php
/**
 * Template for displaying pages
 * 
 * @package delfi
 */

get_header();
while (have_posts()) {
	the_post();
?>
	<div class="block-content">
		<div class="block-inner">
			<div class="text">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
<?php } 
	get_footer();
?>