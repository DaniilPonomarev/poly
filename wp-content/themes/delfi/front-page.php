<?php 
	get_header(); 
	$f = get_fields();
	GLOBAL $site_settings;
	$du = get_template_directory_uri();
if(!isPs()){?>
themes/delfi/front-page.php
<div class="block block-calc step-1" data-step="1" id="block-calc">
	<div class="block-inner">
		<div class="block-inner-2">
			<div class="left">
				<p class="block-title">Конструктор коробок</p>
				<div class="calc-block calc-block-1">
					<p class="cb-title">Выбор материала</p>
					<div class="radio-group">
						<div class="rg-input">
							<input type="radio" name="material" id="material-1" value="сотового полипропилена" checked>
							<span class="rg-left">
								<span class="radio-styler"></span>
							</span>
							<span class="rg-right">
								<span class="rg-title">Сотовый полипропилен</span>
								<span class="rg-subtitle">
									Толщина от 2 до 5мм.
									<br> Плотность от 350 до 1200 гр/м2
								</span>
								<span class="rg-subparams">
									<span class="rg-range">
										<span class="rg-range-title">Толщина,
											<span class="value"><span data-value></span> мм</span>
										</span>
										<input type="range" id="thin-1" class="thin" data-recalc="true" data-values="2,2.5,3,3.5,4,5" value="3.5">
									</span>
									<span class="rg-range">
										<span class="rg-range-title">Плотность, 
											<span class="value"><span data-value></span> гр/м2</span>
										</span>
										<input type="range" id="density-1" class="density">
									</span>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-1" class="lamination" value="без ламинирования" checked="checked" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Без ламинирования</span>
									</label>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-1" class="lamination" value="с ламинированием с одной стороны" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Ламинирование с одной стороны</span>
									</label>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-1" class="lamination" value="с ламинированием с двух сторон" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Ламинирование с двух сторон</span>
									</label>
								</span>

							</span>
						</div>
						<div class="rg-input">
							<input type="radio" name="material" id="material-2" value="Bubble Guard">
							<span class="rg-left">
								<span class="radio-styler"></span>
							</span>
							<span class="rg-right">
								<span class="rg-title">Bubble Guard</span>
								<span class="rg-subtitle">
									Толщина от 3 до 12 мм
									<br> Плотность от 500 до 3500 гр/м2
								</span>
								<span class="rg-subparams">
									<span class="rg-range">
										<span class="rg-range-title">Толщина,
											<span class="value"><span data-value></span> мм</span>
										</span>
										<input type="range" id="thin-2" class="thin" data-recalc="true" data-values="3,4,5,6,7,8,9,10,12">
									</span>
									<span class="rg-range">
										<span class="rg-range-title">Плотность,
											<span class="value"><span data-value></span> мм</span>
										</span>
										<input type="range" id="density-2" class="density">
									</span>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-2" class="lamination" value="без ламинирования" checked="checked" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Без ламинирования</span>
									</label>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-2" class="lamination" value="с ламинированием с одной стороны" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Ламинирование с одной стороны</span>
									</label>
									<label class="rg-checkbox">
										<input type="radio" name="lamination-2" class="lamination" value="с ламинированием с двух сторон" />
										<span class="checkbox-styler"></span>
										<span class="checkbox-title">Ламинирование с двух сторон</span>
									</label>
								</span>

							</span>
						</div>
					</div>
				</div>

				<div class="calc-block calc-block-2">
					<p class="cb-title">Тип коробки</p>

					<label class="rg-input">
						<input type="radio" name="type" value="нескладной" checked=checked>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Нескладываемые</span>
						</span>
					</label>

					<label class="rg-input">
						<input type="radio" name="type" value="складной">
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Складные</span>
						</span>
					</label>
				</div>
				
				<div class="bottom show-mobile--flex">
					 <button type="button" class="show-mobile--ib btn btn-transparent btn-arrow btn-toggle-step btn-transparent-inverse" data-text="01. Выбор материала"
                            >03. Выбор наполнения</button>
				</div>
			</div>
			
			<div class="center">
				<p class="block-title show-mobile--ib">Конструктор коробок</p>
				<div id="calc-image">
				
				

					<div class="layer l-base active">
						<img src="<?=$du?>/assets/images/box/base.png" />
					</div>
					<div class="layer l-handles-short">
						<img src="<?=$du?>/assets/images/box/handles-short.png" />
					</div>
					<div class="layer l-handles-portable">
						<img src="<?=$du?>/assets/images/box/handles-portable.png" />
					</div>
					<div class="layer l-handles-long">
						<img src="<?=$du?>/assets/images/box/handles-long.png" />
					</div>
					
					<div class="layer l-partition-crosswise"><img src="<?=$du?>/assets/images/box/partition-crosswise.png" /></div>
					<div class="layer l-partition-longitudinal"><img src="<?=$du?>/assets/images/box/partition-longitudinal.png" /></div>
					<div class="layer l-partition-all"><img src="<?=$du?>/assets/images/box/partition-all.png" /></div>
					
					<div class="layer l-profile-plastic"><img src="<?=$du?>/assets/images/box/profile-plastic.png" /></div>
					<div class="layer l-profile-aluminum"><img src="<?=$du?>/assets/images/box/profile-aluminum.png" /></div>
					
					<div class="layer l-corners"><img src="<?=$du?>/assets/images/box/corners.png" /></div>
					
					<div class="layer l-sticker"><img src="<?=$du?>/assets/images/box/sticker.png" /></div>
					<div class="layer l-logo"><img src="<?=$du?>/assets/images/box/logo.png" /></div>
				
					<div class="layer l-pocket-short"><img src="<?=$du?>/assets/images/box/pocket-short.png" /></div>
					<div class="layer l-pocket-long"><img src="<?=$du?>/assets/images/box/pocket-long.png" /></div>
					
					<div class="layer l-cap-dustproof"><img src="<?=$du?>/assets/images/box/cap-dustproof.png" /></div>
					<div class="layer l-cap-folding"><img src="<?=$du?>/assets/images/box/cap-folding.png" /></div>
					<div class="layer l-cap-protective"><img src="<?=$du?>/assets/images/box/cap-protective.png" /></div>
					<div class="layer l-cap-cover"><img src="<?=$du?>/assets/images/box/cap-cover.png" /></div>
					
					<div class="layer l-lodgements-h-long h-long"><img src="<?=$du?>/assets/images/box/lodgements-h-long.png" /></div>
					<div class="layer l-lodgements-h-short h-short"><img src="<?=$du?>/assets/images/box/lodgements-h-short.png" /></div>
					<div class="layer l-lodgements"><img src="<?=$du?>/assets/images/box/lodgements.png" /></div>
				</div>
				<div class="bottom flex-wrap">
					<button type="button" class="show-tablet--ib btn btn-transparent btn-arrow btn-toggle-step btn-transparent-inverse" data-text="01. Выбор материала"
					>03. Выбор наполнения</button>
					<a href="/profile" class="show-desktop--ib btn btn-transparent btn-transparent-inverse">Анкета</a>
					<a href="#" class="show-desktop--ib btn btn-transparent btn-transparent-inverse make-order-modal">Оформить заказ</a>
					<a href="#" class="show-desktop--ib btn btn-transparent btn-transparent-inverse do-print">Распечатать</a>
				</div>
			</div>

			<div class="right">
				<button type="button" class="mb-20 show-mobile--ib btn btn-transparent btn-arrow btn-toggle-step btn-transparent-inverse"
                    data-text="01. Выбор материала">03. Выбор наполнения</button>
					
				<div class="calc-block calc-block-3">
					<p class="cb-title">Длина х Ширина х Высота, мм</p>

					<div class="row-inputs">
						<div class="ri-input form-group">
							<input type="text" placeholder="0" name="length" class="form-control">
						</div>
						<span class="divider">x</span>
						<div class="ri-input form-group">
							<input type="text" placeholder="0" name="width" class="form-control">
						</div>
						<span class="divider">x</span>
						<div class="ri-input form-group">
							<input type="text" placeholder="0" name="height" class="form-control">
						</div>
					</div>
				</div>
				<div class="calc-block  calc-block-4">
					<p class="cb-title">Выбор наполнения</p>
					
					<div class="rg-input rg-input__withsubparams">
						<input type="checkbox" name="handles" value="1" id="c-handles" data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Ручки</span>
							<span class="rg-subinputs rg-subinputs__block">
								<label class="rg-checkbox">
									<input type="checkbox" name="handles-long" id="handles-long" value="ручки на длинной стороне короба" 
										data-toggle-layer=".l-handles-long"
										data-exclude="#handles-portable"
									/>
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Ручки на длинной стороне короба</span>
								</label>
								<label class="rg-checkbox">
									<input type="checkbox" name="handles-short" id="handles-short" value="ручки на короткой стороне короба" 
										data-toggle-layer=".l-handles-short" 
										data-exclude="#handles-portable"
									/>
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Ручки на короткой стороне короба</span>
								</label>
								<label class="rg-checkbox">
									<input type="checkbox" name="handles-portable" id="handles-portable" value="ручка переносная" 
										data-toggle-layer=".l-handles-portable" 
										data-exclude="№document_pocket-long,#handles-long,#handles-short,#aluminum-profile-sub,#plastic-profile-sub,#filling-corners,#kr1-protective,#kr1-waybill,#kr1-folding,#kr1-dustproof"
									/>
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Ручка переносная</span>
								</label>
							</span>
						</span>
					</div>
					
					
					
					<div class="rg-input rg-input__withsubparams">
						<input type="checkbox" name="filling" value="1" id="c-perimeter-profile" data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Профиль по периметру</span>
							<span class="rg-subinputs">
								<label class="rg-checkbox">
									<input type="radio" name="c-perimeter-profile-sub" id="aluminum-profile-sub" value="алюминиевый профиль по периметру" data-toggle-layer=".l-profile-aluminum"
										data-exclude="#kr1-waybill,#kr1-folding,#handles-portable" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Алюминиевый</span>
								</label>

								<label class="rg-checkbox">
									<input type="radio" name="c-perimeter-profile-sub" id="plastic-profile-sub" value="пластиковый профиль по периметру" data-toggle-layer=".l-profile-plastic"
										data-exclude="#kr1-waybill,#kr1-folding,#handles-portable" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Пластиковый</span>
								</label>
							</span>
						</span>
					</div>
					
					<label class="rg-input">
						<input type="checkbox" name="corners-cb" value="уголки" id="filling-corners" data-toggle-layer=".l-corners"
							data-exclude="#kr1-waybill,#kr1-folding,#handles-portable" >
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Уголки</span>
						</span>
					</label>
					
					<div class="rg-input rg-input__withsubparams"> <!-- rg-input__filled -->
						<input type="checkbox" name="cap" value="1" id="c-cap" data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Крышка</span>
							<span class="rg-subinputs">

								<label class="rg-checkbox">
									<input type="radio" name="kr1" value="пылезащитная крышка" id="kr1-dustproof"
										id="dustproof" data-toggle-layer=".l-cap-dustproof"
										data-exclude="#handles-portable"
										/>
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Пылезащитная</span>
								</label>

								<label class="rg-checkbox">
									<input type="radio" name="kr1" value="откидная крышка" id="kr1-folding" data-toggle-layer=".l-cap-folding" 
										data-exclude="#aluminum-profile-sub,#plastic-profile-sub,#filling-corners,#handles-portable" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Откидная</span>
								</label>

								<label class="rg-checkbox">
									<input type="radio" name="kr1" value="накладная крышка" id="kr1-waybill" data-toggle-layer=".l-cap-cover"
										data-exclude="#aluminum-profile-sub,#plastic-profile-sub,#filling-corners,#handles-portable" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Накладная</span>
								</label>

								<label class="rg-checkbox">
									<input type="radio" name="kr1" value="защитная крышка из СПП" id="kr1-protective" 
										data-toggle-layer=".l-cap-protective"
										data-exclude="#handles-portable"
										/>
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Защитная из СПП</span>
								</label>
							</span>
						</span>
					</div>
					
					<label class="rg-input">
						<input type="checkbox" name="corners-cb" value="ложементы" id="filling-lodgements" data-toggle-layer=".l-lodgements"
							data-exclude="#partitions_longitudinal,#partitions_crosswise"
							data-hand-short=".l-lodgements-h-short"
							data-hand-long=".l-lodgements-h-long"
							>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Ложементы</span>
						</span>
					</label>
					
					<div class="rg-input rg-input__withsubparams">
						<input type="checkbox" name="document_pocket" value="1" data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Карман для документов</span>
							<span class="rg-subinputs rg-subinputs__block">
								<label class="rg-checkbox">
									<input type="checkbox" name="document_pocket-long" id="document_pocket-long" value="карман на длинной стороне короба" data-toggle-layer=".l-pocket-long" 
										data-exclude="#sticker_sticker,#sticker_logo,#handles-portable" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Карман на длинной стороне короба</span>
								</label>
								<label class="rg-checkbox">
									<input type="checkbox" name="document_pocket-short" value="карман на короткой стороне короба" data-toggle-layer=".l-pocket-short"
										data-exclude="#sticker_sticker,#sticker_logo" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Карман на короткой стороне короба</span>
								</label>
							</span>
						</span>
					</div>
					
					
					<div class="rg-input rg-input__withsubparams">
						<input type="checkbox" name="sticker" value="1" id="c-sticker" data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Стикер</span>
							<span class="rg-subinputs rg-subinputs__block">
								<label class="rg-checkbox">
									<input type="checkbox" name="sticker_sticker" id="sticker_sticker"  value="стикер / наклейка" data-toggle-layer=".l-sticker"
										data-exclude="#document_pocket-long" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Стикер / наклейка</span>
								</label>
								<label class="rg-checkbox">
									<input type="checkbox" name="sticker_logo" id="sticker_logo" value="логотип" data-toggle-layer=".l-logo"
										data-exclude="#document_pocket-long" />
									<span class="checkbox-styler"></span>
									<span class="checkbox-title">Логотип</span>
								</label>
							</span>
						</span>
					</div>
					
					<div class="rg-input rg-input__withsubparams">
						<input type="checkbox" name="partitions-cb" value="1"  data-parent-toggler>
						<span class="rg-left">
							<span class="radio-styler"></span>
						</span>
						<span class="rg-right">
							<span class="rg-title">Перегородки</span>
							<span class="rg-subinputs">
								<span class="row-inputs row-inputs-top">
									<span class="" data-toggle=".ri-input-1">
										<label class="rg-checkbox">
											<input type="checkbox" name="partitions_longitudinal" id="partitions_longitudinal" value="продольные перегородки" 
												data-toggle-layer=".l-partition-longitudinal" 
												data-exclude="#filling-lodgements"
												data-siblings-layer=".l-partition-all" />
											<span class="checkbox-styler"></span>
											<span class="checkbox-title">Продольные</span> 
										</label>
										<span class="ri-input-1 ri-input form-group">
											<input type="text" placeholder="0" class="form-control only-num" name="partitions[transverse]"> 
										</span>
									</span>
									<!--<span class="divider">x</span>-->
									<span class="divider">&nbsp;</span>
									<span class="" data-toggle=".ri-input-2">
										<label class="rg-checkbox">
											<input type="checkbox" name="partitions_crosswise" id="partitions_crosswise" value="поперечные перегородки" 
												data-toggle-layer=".l-partition-crosswise" 
												data-siblings-layer=".l-partition-all" 
												data-exclude="#filling-lodgements"
											/>
											<span class="checkbox-styler"></span>
											<span class="checkbox-title">Поперечные</span>
										</label>
										<span class="ri-input-2 ri-input form-group">
											<input type="text" placeholder="0" class="form-control only-num" name="partitions[longitudinal]">
										</span>
									</span>
								</span>
							</span>
						</span>
					</div>
				</div>

				<a href="#" class="btn btn-transparent btn-transparent-inverse show-mobile show-tablet--ib make-order-modal">Оформить заказ</a>
			</div>
		</div>
	</div>
	<!--noindex-->
	<div class="d-print calc-text">
		<p></p>
	</div>
	<!--/noindex-->
	<div class="bottom show-tablet--flex">
		<a href="#block-services" class="next animate-to">
			Наши услуги
			<img src="/wp-content/themes/delfi/assets/images/arrow-down-dark.svg" alt="">
		</a>
	</div>
</div>
<div class="block block-services" id="block-services">
	<div class="block-inner">
		<<?=$f['services_tag']?> class="block-title"><?=$f['services_title']?></<?=$f['services_tag']?>>
		<?php 
			$services_groups = get_children([
				'post_status' => 'publish', 
				'post_parent' => 34,
				'orderby'     => 'menu_order',
				'order'       => 'ASC',
			]);
		?>
		<ul class="services-menu display-flex flex-wrap">
		<?php foreach($services_groups as $service_group) { ?>
			<li>
				<a class="animate-to" data-offset="20" href="#sg<?=$service_group->ID?>"><?=$service_group->post_title?></a>
			</li>
		<?php } ?>
		</ul>
		<?php foreach($services_groups as $service_group) { ?>
		<div class="service-group" id="sg<?=$service_group->ID?>">
			<p class="sg-title"><?=$service_group->post_title?>:</p>
			<div class="items">	
				<?php 
					$items = get_children([
						'post_status' => 'publish', 
						'post_parent' => $service_group->ID,
						'orderby'     => 'menu_order',
						'order'       => 'ASC',
					]);
					
					foreach($items as $item) {
						$img = get_the_post_thumbnail_url($item, 'thumbnail');
				?>
				<a href="<?=get_the_permalink($item->ID)?>" class="item">
					<span class="img">
						<img src="<?=$img?>" alt="<?=esc_attr($item->post_title)?>">
					</span>
					<span class="title"><?=$item->post_title?></span>
				</a>
				<?php } //$items ?>
			</div>
		</div>

		<?php } //$services_groups ?>
		<?php /*
		<a href="#make-consultation" class="btn btn-transparent fancybox">Консультация</a>
		*/ ?>
	</div>
</div>
<?php } ?>
<div class="block block-features">
	<div class="block-inner">
		<<?=$f['features_tag']?> class="block-title"><?=$f['features_title']?></<?=$f['features_tag']?>>

		<div class="feature-table">
			<div class="center ">
				<img src="<?=$f['features_center_image']['url']?>" alt="<?=esc_attr($f['features_title'])?>">
			</div>
			<?php
				for ($i = 0; $i < count($f['features_items']); $i++) {
					$el1 = $f['features_items'][$i];
					$el2 = $f['features_items'][$i+1];
					++$i; 
			?>
			<div class="line">
				<div class="left">
					<div class="f-item">
						<div class="img">
							<img src="<?=$el1['icon']['url']?>" alt="<?=esc_attr($el1['title'])?>">
						</div>
						<p class="title"><?=$el1['title']?></p>
					</div>
				</div>
				<div class="right">
					<div class="f-item">
						<div class="img">
							<img src="<?=$el2['icon']['url']?>" alt="<?=esc_attr($el2['title'])?>">
						</div>
						<p class="title"><?=$el2['title']?></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<a href="#block-order-form" class="show-desktop--ib btn btn-transparent btn-transparent-inverse animate-to">Оставить заявку</a>
	</div>


</div>
<div class="block block-about" id="block-about">
	<div class="block-inner">
		<<?=$f['about_tag']?> class="block-title"><?=$f['about_title']?></<?=$f['about_tag']?>>
		<div class="block-description">
			<?=$f['about_text']?>
		</div>

		<?php if(!empty($f['about_slider'])) { ?>
		<div class="block-image-slider">
			<div class="swiper-container">
				<div class="inner swiper-wrapper">
					<?php foreach($f['about_slider'] as $item) { ?>
					<div class="item swiper-slide">
						<a href="<?=$item['sizes']['large']?>" data-fancybox="gallery" class="item-inner" 
							style="background-image: url(<?=$item['sizes']['medium_large']?>)"></a>
					</div>
					<?php } ?>
				</div>
				<div class="swiper-pagination"></div>
				<div class="swiper-button-next show-mobile show-tablet"></div>
				<div class="swiper-button-prev show-mobile show-tablet"></div>
			</div>
		</div>
		<?php } ?>


		<?php if(!empty($f['about_btn_text'])) {?>
			<a href="<?=$f['about_btn_link']?>" class="btn btn-transparent"><?=$f['about_btn_text']?></a>
		<?php } ?>
	</div>
</div>
<?php if(!empty($f['box_items'])) { ?>
<div class="block block-boxes">
	<div class="block-inner">
		<?php foreach($f['box_items'] as $item) { ?>
		<<?=!empty($item['link']) ? 'a href="' . get_thepermalink($item['link']) . '"' : 'div'?> class="box-item" style="background-image:url(<?=$item['bg']['sizes']['large']?>)">
			<div class="box-item-inner">
				<div class="content">
					<p class="title"><?=$item['title']?></p>
					<?php if(!empty($item['subtitle'])) { ?>
						<p class="subtitle"><?=$item['subtitle']?></p>
					<?php } ?>
				</div>
			</div>
		</<?=!empty($item['link']) ? 'a' : 'div'?>>
		<?php } ?>
	</div>
</div>
<?php } ?>
<div class="block block-order-form" id="block-order-form">
	<div class="block-inner">
		<<?=$f['order_tag']?> class="block-title"><?=$f['order_title']?></<?=$f['order_tag']?>>
		<p class="block-subtitle"><?=$f['order_subtitle']?></p>
		<!--noindex-->
		<?=do_shortcode($f['order_form'])?>
		<!--/noindex-->
	</div> 
</div>
<?php
	$news_category_id = 3;
	$latest_news = get_posts([
		'posts_per_page' 	=> 3,
		'category'			=> $news_category_id
	]);
	
	if(!empty($latest_news)) {
?>
<div class="block block-news">
	<div class="block-inner">
		<p class="block-title">Свежие новости</p>
		<div class="items">
			<?php 
			foreach($latest_news as $item) { 
				$tags = get_the_tags($item->ID);
				$tagnames = [];
				foreach($tags as $tag) {
					$tagnames[] = $tag->name;
				}
				$date = get_the_date('d F \'y', $item->ID);
			?>
			<a href="<?=get_the_permalink($item->ID)?>" class="item">
				<p class="title"><?=$item->post_title?></p>
				<div class="item-footer">
					<span class="date"><?=$date?></span>
					<?php if(!empty($tags)) { ?>
						<span class="divider">•</span>
						<span class="category"><?=implode(', ', $tagnames)?></span>
					<?php } ?>
				</div>
			</a>
			<?php } ?>
		</div>
		<a href="<?=get_term_link($news_category_id, 'category')?>" class="showall">Открыть все новости</a> 
	</div>
</div>
<?php } ?>
<?php include_once('blocks/_map.php'); ?>
<?php get_footer(); ?>