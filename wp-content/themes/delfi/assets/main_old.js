var body, scrollbarWidth;
$ = jQuery.noConflict();


$(function () {
    body = $('body');
    scrollbarWidth = getScrollBarWidth();

	$('input[type=file]').on('change', function (event) {
		var span = $(event.target).closest('.form-group__fileinput').find('.selected-file > span');
		
        if (event.target.files.length > 0) {
            span.text(event.target.files[0].name);
			$(event.target).closest('.form-group__fileinput').addClass('selected');
        } else {
            span.text(span.data('text'));
			$(event.target).closest('.form-group__fileinput').removeClass('selected');
        }

        files = event.target.files;
    });

	$('.fancybox').each(function(k, v) {
		$(v).click(function() {				
			$.fancybox.open({
				src: $(v).attr('href'),
				animationEffect: "fade",
				type: 'inline',
				touch: false
			});
		})
	})
    /*
    $.fancybox.open({
        src: '#make-order',
        type: 'inline',
        touch: false
    });
    */


    $('.toggle-menu').click(function() {
        body.toggleClass('menu--opened');
        return false;
    })

    $('.text .gallery').each(function (k, v) {
        var gid = $(v).prop('id');
        $.each($(v).find('a'), function (k2, link) {
            //console.log(link, gid);
            $(link).attr('data-fancybox', 'g-' + gid);
        })
    })
  
	$('.text .gallery a').fancybox();

    $(window).on('resize', function () {
        scrollbarWidth = getScrollBarWidth();
    })

    $('.text table').wrap('<div class="table-responsive"></div>')

    $('.block-image-slider .swiper-container').each(function (k, v) {
        var swiper = new Swiper($(v), {
            slidesPerView: 5,
            spaceBetween: 45,
            //centeredSlides: true, 
            slidesPerGroup: 5,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: $(v).find('.swiper-button-next'),
                prevEl: $(v).find('.swiper-button-prev'),
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    slidesPerGroup: 3
                },
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1
                }
            }
        });
    })

    $('.block-calc .btn-arrow').click(function () {
        $(this).closest('.block-calc').removeClass('step-1').addClass('step-2');
    })

    $('a.animate-to, .animate-to > a').click(function () {
        var href = trim($(this).attr('href'), '/');

        if ($(href).get(0)) {
            $("html, body").animate({
                scrollTop: $(href).offset().top
            }, 1000);
            return false;
        }
    })

})


function trim (s, c) {
  if (c === "]") c = "\\]";
  if (c === "\\") c = "\\\\";
  return s.replace(new RegExp(
    "^[" + c + "]+|[" + c + "]+$", "g"
  ), "");
}


function setupContactMap() {
    var map = jQuery('#contacts-map');

    var center = new google.maps.LatLng(map.data('lat'), map.data('lng'));
    var styledMapType = new google.maps.StyledMapType(map_styles, {
		name:'plsm_style'
	});
	 
    gmap = new google.maps.Map(map.get(0), {
        center: center,
        zoom: map.data('zoom'),
        mapTypeControl: false,
        scaleControl: false,
        mapTypeControlOptions: {
            mapTypeIds: ['plsm_style']
        }
    });
	
	var offsetx = 0;
	var offsety = -200;
	
    google.maps.event.addDomListener(window, 'resize', function () {
        gmap.setCenter(center);
    });
    gmap.mapTypes.set('plsm_style', styledMapType);
    gmap.setMapTypeId('plsm_style');
	
	var marker = new google.maps.Marker({
		position: center,
		icon: '/wp-content/themes/delfi/assets/images/logo_blue.svg',
		map: gmap
	});
	
	
	var infowindow = new google.maps.InfoWindow({
		content: '<p class="map_info_window">Наш адрес: <b>' + map.data('address') + '</b></p>'
	});
	infowindow.open(gmap, marker);
	
	gmap.setCenterWithOffset(center, 0, -180);
	
	setTimeout(function() {
		$('.map_info_window').closest('.gm-style-iw').parent().addClass('blue-balloon') 		
	}, 500)
}


function initMap() {
	google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
		var map = this;
		var ov = new google.maps.OverlayView();
		ov.onAdd = function() {
			var proj = this.getProjection();
			var aPoint = proj.fromLatLngToContainerPixel(latlng);
			aPoint.x = aPoint.x+offsetX;
			aPoint.y = aPoint.y+offsetY;
			map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
		}; 
		ov.draw = function() {}; 
		ov.setMap(this); 
	};

    if (jQuery('#contacts-map').get(0)) {
        setupContactMap();
    }
}


function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
};






var map_styles = [{
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#444444"
        }]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
            "color": "#f2f2f2"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [{
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
                "color": "#b4d4e1"
            },
            {
                "visibility": "on"
            }
        ]
    }
];