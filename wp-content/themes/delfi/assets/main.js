var body, scrollbarWidth, getCanvas;
$ = jQuery.noConflict();

function rg(g) {
    if (typeof yaCounter49284364 !== 'undefined') {
        console.log('rg', g);
        yaCounter49284364.reachGoal(g);
    }
}

document.addEventListener('wpcf7mailsent', function( event ) {
	if ( '169' == event.detail.contactFormId || '173' == event.detail.contactFormId ) {
		rg('Submitting-the-Consultation-Form');
	}
	if ( '161' == event.detail.contactFormId ) {
		rg('Sending-the-form-Checkout');
	}
	if ( '170' == event.detail.contactFormId ) {
		rg('Submitting-the-form-Send-a-request');
	}
}, false );

function dataURItoBlob(dataURI) {			
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

$(function () {
	$('body').on('click', '.block-header .block-contacts .phone:not(.phone__email) a', function() {
		rg('Click-on-the-phone-number');
	})	
	
	$('body').on('click', '.block-header .block-contacts .phone__email a', function() {
		rg('Click-to-email-address');
	})
	
	$('body').on('click', '[href="#make-consultation"]', function() {
		rg('Click-on-the-button-Consultation');
	})
	
	//$.fancybox.defaults.hideScrollbar = false;
    body = $('body');
    scrollbarWidth = getScrollBarWidth();
	
	$('.do-print').click(function() {
		rg('Click-on-the-button-Print');
		$('.modal-order .box-cols .col-2 .col-inner .col-text, .calc-text p').html(generate_box_text());
		window.print();
		return false;
	})
	
	$('div.rg-input').each(function(k, v) {
		var clsName = v.className;
		$from = $(v);
		$from.replaceWith($('<label/>').addClass(clsName).html($from.html()));
	})

    $('.only-num').on('keypress', function (evt) {
        if (evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    })

    $('input[type=file]').on('change', function (event) {
        var span = $(event.target).closest('.form-group__fileinput').find('.selected-file > span');

        if (event.target.files.length > 0) {
            span.text(event.target.files[0].name);
            $(event.target).closest('.form-group__fileinput').addClass('selected');
        } else {
            span.text(span.data('text'));
            $(event.target).closest('.form-group__fileinput').removeClass('selected');
        }

        files = event.target.files;
    });

    $('.fancybox').each(function (k, v) {
        $(v).click(function () {
            $.fancybox.open({
                src: $(v).attr('href'),
                animationEffect: "fade",
                type: 'inline',
                touch: false
            });
        })
    })
	
	$('.make-order-modal').click(function() {
		rg('Click-on-the-button-Checkout');
		$.fancybox.open({
			src: '#make-order',
			type: 'inline',
			touch: false
		});
		
		if(typeof html2canvas != 'undefined') {
			html2canvas($('#calc-image').get(0)).then(function(canvas) {
				//$('.image-preview').html(canvas);
				//$('[name=box]').val(canvas.toDataURL())
				//document.body.prepend(canvas);
				var photo = canvas.toDataURL('image/jpeg');                
				$.post('/save_box.php', {photo: canvas.toDataURL()} , function(response) {
					$('.box-image').val(location.protocol + '//' + location.hostname + response.file);
				})
			});
		}
		
		$('#order-box').html($('#calc-image').html());
		$('.modal-order .box-cols .col-2 .col-inner .col-text, .calc-text p').html(generate_box_text());
		$('[name=your-order]').val(strip(generate_box_text()));
		
		return false;
	})

    $('.toggle-menu').click(function () {
        body.toggleClass('menu--opened');
        return false;
    });
	
	$('.block-menu .middle-panel ul li a').click(function() {
		body.removeClass('menu--opened');
		return true;
	})

    $('.text .gallery').each(function (k, v) {
        var gid = $(v).prop('id');
        $.each($(v).find('a'), function (k2, link) {
            //console.log(link, gid);
            $(link).attr('data-fancybox', 'g-' + gid);
        })
    })

    $('.text .gallery a').fancybox();

    $(window).on('resize', function () {
        scrollbarWidth = getScrollBarWidth();
    })

    $('.text table').wrap('<div class="table-responsive"></div>')

    $('.block-image-slider .swiper-container').each(function (k, v) {
        var swiper = new Swiper($(v), {
            slidesPerView: 5,
            spaceBetween: 45,
            //centeredSlides: true, 
            slidesPerGroup: 5,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: $(v).find('.swiper-button-next'),
                prevEl: $(v).find('.swiper-button-prev'),
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    slidesPerGroup: 3
                },
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1
                }
            }
        });
    })

    $('.block-calc .btn-toggle-step').click(function () {
        var from_step = 'step-1',
            to_step = 'step-2',
            remove_class = 'btn-arrow',
            add_class = 'btn-arrow--back'

        if ($(this).closest('.block-calc').hasClass('step-2')) {
            from_step = 'step-2'
            to_step = 'step-1'
            remove_class = 'btn-arrow--back'
            add_class = 'btn-arrow'
        }

        $(this).closest('.block-calc')
            .removeClass(from_step).addClass(to_step)

        $('.btn-toggle-step').each(function (k, v) {
            var tmp = $(v).text();
            $(v).text($(v).data('text'));
            $(v).data('text', tmp)
        }).removeClass(remove_class).addClass(add_class)
    })

    $('a.animate-to, .animate-to > a').click(function () {
        var href = trim($(this).attr('href'), '/');
		var offsetTop = $(this).data('offset') || 0;
		
        if ($(href).get(0)) {
            $("html, body").animate({
                scrollTop: $(href).offset().top - offsetTop
            }, 1000);
            return false;
        }
    })

})

$('body').on('click', '[data-print-link]', function() {
	printRemote($(this).attr('href'));
	return false;
})

function printRemote(url) {
	var _this = this,
		iframeId = 'iframeprint';

	jQuery('iframe#' + iframeId).remove();
	$iframe = jQuery("<iframe id=\"" + iframeId + "\" src=\"" + url + "\" />");

	$iframe.load(function() {
		window.console.log(2);
		_this.callPrint(iframeId);

	});
	$iframe.css('display', 'none').appendTo($('body'));
}

function strip(html) {
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	return tmp.textContent || tmp.innerText;
}

function generate_box_text() {
	response = '<p>'
	response += 'Вы сконструировали ' + $('[name=type]:checked').val() + ' короб из '
	response += $('[name=material]:checked').val();
	response += ' ' + $('[name=material]:checked').closest('.rg-input').find('.lamination:checked').val() + ', ';
	
	response += 'толщиной ' + $('[name=material]:checked').closest('.rg-input').find('.thin:input').data("ionRangeSlider").result.from_value + '&nbsp;мм '
	response += 'и плотностью ' + $('[name=material]:checked').closest('.rg-input').find('.density:input').data("ionRangeSlider").result.from_value + '&nbsp;гр/м2'
	
	length = $('[name=length]').val() == '' ? 0 : parseFloat($('[name=length]').val());
	width  = $('[name=width]').val() == '' 	? 0 : parseFloat($('[name=width]').val());
	height  = $('[name=height]').val() == '' ? 0 : parseFloat($('[name=height]').val());
	
	response += '<br>Размеры ДxШxВ: ' + length + 'x' +  width + 'x' + height + ' мм.';
	
	
	filling = [];
	$.each($('.rg-input'), function(k, rginput) {
		if($(rginput).hasClass('rg-input__withsubparams')) {
			var rgsubinputs = $(rginput).find('.rg-subinputs');
			
			if($(rgsubinputs).find('[type=checkbox]:checked').length) {
				$(rgsubinputs).find('[type=checkbox]:checked').each(function(k, v) {
					count = $(v).closest('[data-toggle]').find('[type=text]:input');
					res = $(v).val();
					if(count.length) {
						count = count.val() == '' ? 0 : parseFloat(count.val());
						res += ' (' + count + ' шт)';
					}
					filling.push(res);
				})
			} else if($(rgsubinputs).find('[type=radio]:checked').length) {
				filling.push($(rgsubinputs).find('[type=radio]:checked').val());
			}
			
		} else {
			$el = $(rginput).find('[type=checkbox]:checked')
			if($el.length) {
				filling.push($el.val())
			}
		}
	})
	
	if(filling.length) {
		response += ' <br>Наполнение: ' + filling.join(', ');
	}
	
	
	/*
	<p>Вы сконструировали короб из:
		<span data-box-type>листового полипропилена</span> толщиной
		<span>13мм</span> и плотностью
		<span>0.5</span>, с уголками из
		<span>алюминия</span> и др.</p>
	*/
	response += '</p>'
	return response;
}


function trim(s, c) {
    if (c === "]") c = "\\]";
    if (c === "\\") c = "\\\\";
    return s.replace(new RegExp(
        "^[" + c + "]+|[" + c + "]+$", "g"
    ), "");
}


function setupContactMap() {
    var map = jQuery('#contacts-map');

    var center = new google.maps.LatLng(map.data('lat'), map.data('lng'));
    var styledMapType = new google.maps.StyledMapType(map_styles, {
        name: 'plsm_style'
    });

    gmap = new google.maps.Map(map.get(0), {
        center: center,
        zoom: map.data('zoom'),
        mapTypeControl: false,
        scaleControl: false,
        mapTypeControlOptions: {
            mapTypeIds: ['plsm_style']
        }
    });

    var offsetx = 0;
    var offsety = -180;

    google.maps.event.addDomListener(window, 'resize', function () {
        gmap.setCenter(center);
    });
    gmap.mapTypes.set('plsm_style', styledMapType);
    gmap.setMapTypeId('plsm_style');

    var marker = new google.maps.Marker({
        position: center,
        icon: new google.maps.MarkerImage('/wp-content/themes/delfi/assets/images/logo_blue.svg', null, null, null, new google.maps.Size(120,97)),
        map: gmap
    });


    var infowindow = new google.maps.InfoWindow({
        content: '<p class="map_info_window">Наш адрес: <b>' + map.data('address') + '</b></p>'
    });
    infowindow.open(gmap, marker);

    gmap.setCenterWithOffset(center, 0, offsety);

    setTimeout(function () {
        $('.map_info_window').closest('.gm-style-iw').parent().addClass('blue-balloon')
    }, 1000)
}


function initMap() {
    google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
        var map = this;
        var ov = new google.maps.OverlayView();
        ov.onAdd = function () {
            var proj = this.getProjection();
            var aPoint = proj.fromLatLngToContainerPixel(latlng);
            aPoint.x = aPoint.x + offsetX;
            aPoint.y = aPoint.y + offsetY;
            map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
        };
        ov.draw = function () {};
        ov.setMap(this);
    };

    if (jQuery('#contacts-map').get(0)) {
        setupContactMap();
    }
}


function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
};






var map_styles = [{
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#444444"
        }]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
            "color": "#f2f2f2"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [{
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [{
            "visibility": "simplified"
        }]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
                "color": "#b4d4e1"
            },
            {
                "visibility": "on"
            }
        ]
    }
];