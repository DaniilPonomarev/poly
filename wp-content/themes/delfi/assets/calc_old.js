var calcConfig, temp;
$ = jQuery.noConflict();

function checkCalc() {
	$.each(calcConfig, function(confKey, conf) { // Each conf
		if("range" in conf) {
			$.each(conf.range, function(rangeKey, range) {
				var srcSlider = conf.src.data("ionRangeSlider");
				
				if(range.if.indexOf(parseFloat(srcSlider.result.from_value)) !== -1) {
					var slider = conf.dst.data("ionRangeSlider");
					slider.update(range.then);
					//console.log(range.then);
					//updateGrid(slider);
					if('from' in range.then) {
						//conf.dst.closest('.rg-range').find('[data-value]').text(range.then.from);
						conf.dst.closest('.rg-range').find('[data-value]').text(conf.dst.data("ionRangeSlider").result.from_value);
					}
					return false;
				}
			})
		}
	})
	
	syncRangeSliderValues();
}

function syncRangeSliderValues() {
	$('.block-calc input[type=range]').each(function(k, input) {
		var slider = $(input).data("ionRangeSlider"),
			spanValue = $(input).closest('.rg-range').find('[data-value]');
		
		if(spanValue.length) {
			spanValue.text(slider.result.from_value ? slider.result.from_value : slider.result.from)
		}
	})
}

function updateGrid(slider) {
	slider.update({
		grid_num : parseFloat((slider.options.max - slider.options.min) / slider.options.step)
	});
}

function toggleLayer(element, action) {
	var action = action || 'auto';
	var layerClass = element.data('toggle-layer');
	
	if((action == 'auto' && element.prop('checked') == true) || action == "show") {	
		$(layerClass).addClass('active');
	} else {
		$(layerClass).removeClass('active');
	}
}

$(function() {
	
	$('[data-parent-toggler]').change(function() {
		//console.log($(this).find('[data-toggle-layer]'));
		if($(this).prop('checked') == false) {
			$(this).closest('.rg-input').find('[data-toggle-layer]').prop('checked', false).trigger('change');
		}
	})
	
	
	/*
	$('.rg-subinputs ').on('click', ':radio:checked', function() {
		console.log(1, $(this).prop('checked'), $(this).attr('checked'));
		if($(this).prop('checked') == false) {
			$(this).closest('.rg-input__withsubparams').find('[data-parent-toggler]').prop('checked', false).trigger('change');
		}
	})
	*/
	
	$('[data-toggle-layer]').change(function() {
		console.log(2, $(this).prop('checked'), $(this).attr('checked'));
		var $this = $(this);
		
		if($this.closest('[data-toggle]') && $($this.closest('[data-toggle]').data('toggle')).length) {
			var parentConteiner = $($this.closest('[data-toggle]').data('toggle'));
			if(($this).prop('checked') == true) {
				parentConteiner.addClass('active');
			} else {
				parentConteiner.removeClass('active');
			}
		}
		
		var sub2 = $(this).closest('.rg-subinputs');
		if($this.data('siblings-layer') && $($this.data('siblings-layer')).length && sub2.length) {
			if(sub2.find(':checkbox,:radio').length == sub2.find(':checkbox:checked,:radio:checked').length) {
				$($this.data('siblings-layer')).addClass('active');
			} else {
				$($this.data('siblings-layer')).removeClass('active');
			}
		}
		
		toggleLayer($this);
		
		if($this.data('exclude') && $this.prop('checked') == true) {
			var exclude = $this.data('exclude').split(',');
			$.each(exclude, function(k, v) {
				if($(v).length && $(v).prop('checked') == true) {
					$(v).prop('checked', false);
					sub = $(v).closest('.rg-subinputs').find(':input:checked');
					if(sub.length == 0) {
						$(v).closest('.rg-input__withsubparams').find('[data-parent-toggler]').prop('checked', false);
					}
					toggleLayer($(v), 'hide');
				}
			})
		}
		
		if($(this).attr('type') == 'radio') {
			$(this).closest('.rg-input').find('[data-toggle-layer]').not($this).each(function(k, inp) {
				toggleLayer($(inp), 'hide');
			})
		}
	})
	
	//Material
	//$('.calc-block-1 ')
	
	
	$('[name=material]').change(function() {
		setTimeout(function() {
			checkCalc();
		}, 300)
	})
	
	calcConfig = [
		{
			'src': $('#thin-1'),
			'dst': $('#density-1'),
			'range': [
				{
					'if': [2],
					'then': {
						//'min': 350,
						//'max' : 500,
						'values' : [350, 400, 450, 500],
						'from': 1
					}
				},
				{
					'if': [2.5],
					'then': {
						//'min': 400,
						//'max' : 500,
						'values':  [400, 450, 500],
						'from': 1
					}
				},
				{
					'if': [3],
					'then': {
						//'min': 500,
						//'max' : 800,
						'values': [500, 600, 700, 800],
						'from': 1
					}
				},
				{
					'if': [3.5],
					'then': {
						//'min': 600,
						//'max' : 1200,
						'values': [700, 800, 900, 1000],
						'from': 0
					}
				},
				{
					'if': [4],
					'then': {
						//'min': 600,
						//'max' : 1200,
						'values': [850, 900, 950, 1000],
						'from': 0
					}
				},
				{
					'if': [5],
					'then': {
						//'min': 600,
						//'max' : 1200,
						'values': [1000, 1100, 1200, 1300 ],
						'from': 0
					}
				}
			]
		},
		{
			'src': $('#thin-2'),
			'dst': $('#density-2'),
			'range': [
				{
					'if': [3],
					'then': {
						//'min': 500,
						//'max' : 700,
						'values': [500, 550, 600, 650, 700],
						'from': 1
					}
				},
				{
					'if': [4],
					'then': {
						//'min': 500,
						//'max' : 700,
						'values': [500, 550, 600, 650, 700],
						'from': 4
					}
				},
				{
					'if': [5],
					'then': {
						//'min': 700,
						//'max' : 1300,
						'values': [800, 850, 900, 950, 1000, 1200, 1500],
						'from': 4
					}
				},
				{
					'if': [6],
					'then': {
						//'min': 900,
						//'max' : 1500,
						'values': [800, 850, 900, 950, 1000, 1200, 1500],
						'from': 5
					}
				},
				{
					'if': [7],
					'then': {
						//'min': 900,
						//'max' : 1500,
						'values': [950, 1000, 1500, 1800],
						'from': 1
					}
				},
				{
					'if': [8],
					'then': {
						//'min': 1500,
						//'max' : 2700,
						'values': [950, 1000, 1500, 1800],
						'from': 3
					}
				},
				{
					'if': [9],
					'then': {
						//'min': 1500,
						//'max' : 2700,
						'values': [2500, 2700, 3000],
						'from': 0
					}
				},
				{
					'if': [10],
					'then': {
						//'min': 1500,
						//'max' : 2700,
						'values': [2500, 2700, 3000],
						'from': 2
					}
				},
				{
					'if': [12],
					'then': {
						//'min': 2500,
						//'max' : 3500,
						'values': [3000, 3600],
						'from_value': 0
					}
				}
			]
		}
	]

	
	if (typeof $.fn.ionRangeSlider != 'undefined') {
        $('input[type="range"]').each(function (k, inp) {
			params = {
                grid: true,
                onStart: function (data) {
					//console.log('start', data, data.from_value)
					//$(data.input).closest('.rg-range').find('[data-value]').text(data.from_value ? data.from_value : data.input.val());
                },
                onChange: function (data) {
					//console.log('change', data);
                    //$(data.input).closest('.rg-range').find('[data-value]').text(data.input.val());
                    //$(data.input).closest('.rg-range').find('[data-value]').text(data.from_value);
					if(data.input.data('recalc')) {
						checkCalc();
					} else {
						syncRangeSliderValues();
					}
                }
            };
			
			if($(inp).data('values')) {
				params.values = $(inp).data('values').split(',').map(x => parseFloat(x));
				params.grid_num = params.values.length;
			} else {
				params.grid_num = parseFloat(($(inp).prop('max') - $(inp).prop('min')) / $(inp).prop('step'));
				params.min = $(inp).prop('min');
                params.max = $(inp).prop('max');
				params.step = $(inp).prop('step');
			}
			//console.log(params);
            $(inp).ionRangeSlider(params);
        })

    }
	checkCalc();
}) 


